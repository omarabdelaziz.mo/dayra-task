<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Welcome + name
                </div>
                <div class="d-flex flex-row justify-content-end mr-3">
                    <a href="{{ route('client.index')}}" class="btn btn-outline-primary mr-4">Clients</a>
                    <a href="{{ route('dashboard')}}" class="btn btn-primary">Innvoices</a>
                </div>
       
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
    
                                    <div class="clearfix"></div>
                                        <div class="d-flex flex-row justify-content-end">
                                            <a href="{{ route('client.create') }}" class="btn btn-outline-success pull-right" style="float: right;margin-bottom: 10px; margin-right: 10px">Add Client</a>

                                            <a href="{{ route('invoice.create') }}" class="btn btn-success pull-right" style="float: right;margin-bottom: 10px">Add Invoice</a>
                                        </div>
                                        <div class="tab-content">
                                           
                                            <div id="approved_request" class="tab-pane fade show active">
                                                <table id="datatable-buttons"
                                                        class="table table-striped table-bordered dt-responsive nowrap datatableFormating"
                                                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Client ID</th>
                                                        <th>Full Name</th>
                                                        <th>E-Mail</th>
                                                        <th>Phone Number</th>
                                                    </tr>
                                                    </thead>
    
                                                    <tbody>
                                                        @foreach ($clients as $client)
                                                            <tr>
                                                                <td>{{$client['id']}}</td>
                                                                <td>{{ $client['full_name']}}</td>
                                                                <td>{{ $client['email']}}</td>
                                                                <td>{{ $client['mobile']}}</td>
                                                            </tr>
                                                        @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                                
                                            </div> 
                                        </div>      
                                </div>

                            </div> <!-- end col -->
                        </div> <!-- end row -->
    
                    </div>
                <!-- end container-fluid -->
                </div>
            </div>
        </div>
    </div>

    <!--  Modal Active -->
    <div id="createNew" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header" dir="rtl">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create invoice</h4>
                </div>
                <div class="modal-body">
                    <p id="active_mark" style="margin-left:40%;"></p>
                    <br>
                    <div>

                        <form method="post" action="/games" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="titleid" class="col-sm-3 col-form-label">Game Title</label>
                                <div class="col-sm-9">
                                    <input name="title" type="text" class="form-control" id="titleid" placeholder="Game Title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="publisherid" class="col-sm-3 col-form-label">Game Publisher</label>
                                <div class="col-sm-9">
                                    <input name="publisher" type="text" class="form-control" id="publisherid"
                                           placeholder="Game Publisher">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="releasedateid" class="col-sm-3 col-form-label">Release Date</label>
                                <div class="col-sm-9">
                                    <input name="releasedate" type="text" class="form-control" id="releasedateid"
                                           placeholder="Release Date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gameimageid" class="col-sm-3 col-form-label">Game Image</label>
                                <div class="col-sm-9">
                                    <input name="image" type="file" id="gameimageid" class="custom-file-input">
                                    <span style="margin-left: 15px; width: 480px;" class="custom-file-control"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-primary">Submit Game</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>
</x-app-layout>
