<x-app-layout>


    <div class="py-12">
        <div class="container">
        <h4 class="h1">Add new Client</h4>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{ route('client.store') }}" style="width: 50%">
            {{ csrf_field() }}
            <div class="mb-3">
                <label for="emaildId" class="form-label">Client Email</label>
                    <input name="email" type="email" class="form-control" id="emaildId" placeholder="Client Email">
            </div>
            <div class="mb-3">
                <label for="nameId" class="form-label">Client Name</label>
                    <input name="full_name" type="text" class="form-control" id="nameId"
                            placeholder="Client Name">
            </div>
            <div class="mb-3">
                <label for="phoneId" class="form-label">Phone Number</label>
                    <input name="mobile" type="text" class="form-control" id="phoneId"
                            placeholder="Client Phone">
            </div>
        
            
            <div class="mb-3">
                <div class="offset-sm-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Add Client</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</x-app-layout>
