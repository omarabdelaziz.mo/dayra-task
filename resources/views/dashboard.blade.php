<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Welcome {{ Auth::user()->name}}
                </div>
                <div class="d-flex flex-row justify-content-end mr-3">
                    <a href="{{ route('client.index')}}" class="btn btn-outline-primary mr-4">Clients</a>
                    <a href="{{ route('dashboard')}}" class="btn btn-primary">Innvoices</a>
                </div>
       
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
    
                                    <div class="clearfix"></div>
                                        <div class="d-flex flex-row justify-content-end">
                                            <a href="{{ route('client.create') }}" class="btn btn-outline-success pull-right" style="float: right;margin-bottom: 10px; margin-right: 10px">Add Client</a>

                                            <a href="{{ route('invoice.create') }}" class="btn btn-success pull-right" style="float: right;margin-bottom: 10px">Add Invoice</a>
                                        </div>
                                        <div class="tab-content">
                                           
                                            <div id="approved_request" class="tab-pane fade show active">
                                                <table id="datatable-buttons"
                                                        class="table table-striped table-bordered dt-responsive nowrap datatableFormating"
                                                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Invoice ID</th>
                                                        <th>Full Name</th>
                                                        <th>E-Mail</th>
                                                        <th>Phone Number</th>
                                                        <th>Amount</th>
                                                        <th>Due At</th>
                                                    </tr>
                                                    </thead>
    
                                                    <tbody>
                                                        @foreach ($invoices as $invoice)
                                                            <tr>
                                                                <td>{{$invoice['id']}}</td>
                                                                <td>{{ $invoice['client']['full_name']}}</td>
                                                                <td>{{ $invoice['client']['email']}}</td>
                                                                <td>{{ $invoice['client']['mobile']}}</td>
                                                                <td>{{$invoice['amount']}}</td>
                                                                <td>{{ $invoice['due_at']}} </td>
                                                            </tr>
                                                        @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                                
                                            </div> 
                                        </div>      
                                </div>

                            </div> <!-- end col -->
                        </div> <!-- end row -->
    
                    </div>
                <!-- end container-fluid -->
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
