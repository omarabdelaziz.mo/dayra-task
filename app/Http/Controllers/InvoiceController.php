<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use App\Models\Client;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    public function create(Request $request)
    {
        return view('invoice.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'full_name' => 'required',
            'mobile'=>'required|regex:/(01)[0-9]{9}/',
            'amount'=>'required|numeric',
            'due_at' => 'required'
        ]);
        $data = ($request->all());
        $client = $this->get_client($data);
        if(empty($client))
        {
            $client = Client::create($data);
            Mail::to($client['email'])->send(new WelcomeMail($client));

        }
        $data['client_id'] = $client->id;
        $invoice = Invoice::create($data);
        return redirect()->route('dashboard');
    }
    private function get_client($data)
    {
        return Client::where('email',$data['email'])->orWhere('mobile', $data['mobile'])->first();
    }
}
