<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $invoices = Invoice::with('client')->get()->toArray();
        return view('dashboard', compact('invoices'));
    }
}
