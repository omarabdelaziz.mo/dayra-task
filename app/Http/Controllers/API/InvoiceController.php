<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceCollection;
use App\Mail\WelcomeMail;
use App\Models\Client;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoices = Invoice::with('client')->get();
        return new InvoiceCollection($invoices);
    }

    public function store(Request $request)
    {
        $validated  = $this->validate_invoice($request);
        if($validated->fails())
        {
            $errors['status'] = false;
            $errors['message'] = $validated->errors();
            return response()->json($errors,  Response::HTTP_BAD_REQUEST);
        }
        $data = ($request->all());
        $client = $this->get_client($data);
        if(empty($client))
        {
            $client = Client::create($data);
            Mail::to($client['email'])->send(new WelcomeMail($client));
        }
        $data['client_id'] = $client->id;
        $invoice = Invoice::create($data);
        return new InvoiceCollection(Invoice::with('client')->where('id', $invoice->id)->get());
    }

    private function get_client($data)
    {
        return Client::where('email',$data['email'])->orWhere('mobile', $data['mobile'])->first();
    }
    
    private function validate_invoice($request)
    {
        return  Validator::make($request->all(),[
            'email' => 'required|email',
            'full_name' => 'required',
            'mobile'=>'required|regex:/(01)[0-9]{9}/',
            'amount'=>'required|numeric',
            'due_at' => 'required'
        ]);
        
    }



    public function show($id)
    {
        return new InvoiceCollection(Invoice::where('id',$id)->with('client')->get());
    }
}
