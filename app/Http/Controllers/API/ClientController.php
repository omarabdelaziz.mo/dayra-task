<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ClientCollection;
use App\Mail\WelcomeMail;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    
    public function index()
    {
        $invoices = Client::with('invoice')->get();
        return new ClientCollection($invoices);
    }

    public function store(Request $request)
    {
        $validated  = $this->validate_invoice($request);
        if($validated->fails())
        {
            $errors['status'] = false;
            $errors['message'] = $validated->errors();
            return response()->json($errors,  HttpResponse::HTTP_BAD_REQUEST);
        }
        $client = Client::create($request->all());
        Mail::to($client['email'])->send(new WelcomeMail($client));

        return new ClientCollection(Client::where('id', $client->id)->with('invoice')->get());
    }

    
    private function validate_invoice($request)
    {
        return  Validator::make($request->all(),[
            'email' => 'required|email|unique:clients,email',
            'full_name' => 'required',
            'mobile' => 'required|regex:/(01)[0-9]{9}/|unique:clients,mobile'
        ]);
        
    }

    public function show($id)
    {
        return new ClientCollection(Client::where('id',$id)->with('invoice')->get());
    }
}
