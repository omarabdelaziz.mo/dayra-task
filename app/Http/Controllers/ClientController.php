<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    
    public function create(Request $request)
    {
        return view('client.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email|unique:clients,email',
            'full_name' => 'required',
            'mobile' => 'required|regex:/(01)[0-9]{9}/|unique:clients,mobile'
        ]);
        $client = Client::create($request->all());
        Mail::to($client['email'])->send(new WelcomeMail($client));

        return redirect()->route('client.index');
    }
    public function index()
    {
        $clients = Client::get()->toArray();
        return view('client.index', compact('clients'));
    }
}
